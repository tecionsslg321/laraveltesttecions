<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Customer\ManageCustomer@ViewCustomerPage')->name('index.page');
Route::post('/', 'Customer\ManageCustomer@SaveaCustomer')->name('index.save');
Route::get('/customer', 'Customer\ManageCustomer@Viewallcustomer')->name('index.all');

