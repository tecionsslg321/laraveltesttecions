<?php

namespace App\Http\Controllers\Customer;

use App\Model\CreateCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageCustomer extends Controller
{
   public function ViewCustomerPage(){
       return view('page.customer.createcustomer');
   }

   public function  SaveaCustomer(Request $request){
       $savecustomer = new CreateCustomer();

       $savecustomer->name = $request->name;
       $savecustomer->age = $request->age;
       $savecustomer->address = $request->address;
       $savecustomer->phone = $request->phone;
       $savecustomer->email = $request->email;
       $savecustomer->save();

       return back();



   }


   public function Viewallcustomer()
   {
       $allcustomer = CreateCustomer::all();

       return view('page.customer.allcustomer')->with([
           'allcustomer' => $allcustomer
       ]);



   }

}
